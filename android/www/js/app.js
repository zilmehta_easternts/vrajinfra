var vrajApp = angular.module('vrajApp', ["ngRoute", "ngStorage", "mobile-angular-ui", "ui.bootstrap"]);

var serviceurl = "http://www.vrajinfra.com/data/";

/* to display special characters*/
vrajApp.filter('html', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
})

function openlinks(url) {
    //alert(url);
    window.open(url, "_system");
}

function onBackKeyDown() {
    navigator.app.exitApp();
}

function removeHTMLTags() {
    var strInputCode = document.getElementById("overviewid").innerHTML;
    strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1) {
        return (p1 == "lt") ? "<" : ">";
    });
    var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
    return strTagStrippedText;
}

vrajApp.config(function ($routeProvider) {
    $routeProvider
    // route for the home page
        .when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'mainController',
            label: 'Home'
        })
        .when('/success-story', {
            templateUrl: 'partials/success-story.html',
            controller: 'success-storyController'
        })
        .when('/newsevents', {
            templateUrl: 'partials/news.html',
            controller: 'newsController'
        })
        .when('/allprojects/', {
            templateUrl: 'partials/allprojects.html',
            controller: 'allprojectController',
            label: 'Projects'
        })
        .when('/inquiry', {
            templateUrl: 'partials/contact-us.html',
            controller: 'contactController',
            label: 'Contact Us'
        })
        .when('/about', {
            templateUrl: 'partials/about.html',
            controller: 'aboutController'
        })
        .when('/ProjectSelect/:projectId', {
            templateUrl: 'partials/projects.html',
            controller: 'projectController',
            label: 'Projects'
        })
        .when('/searchprojects', {
            templateUrl: 'partials/searchprojects.html',
            controller: 'searchprojectsController',
            label: 'Search Projects'
        })
        .when('/search/:id', {
            templateUrl: 'partials/search.html',
            controller: 'searchController',
            label: 'Search Projects'
        })
        .when('/Project/:projectId', {
            templateUrl: 'partials/project-detail.html',
            controller: 'projectDetailController',
            label: 'Projects'
        })
        .when('/contact', {
            templateUrl: 'partials/reachus.html',
            controller: 'reachusController',
            label: 'Reach Us'
        })
        .when('/subscribe', {
            templateUrl: 'partials/subscribe.html',
            controller: 'subscribeController'
        })
        .when('/notification', {
            templateUrl: 'partials/notification.html',
            controller: 'notificationController',
            label: 'Notifications'
        })
        .otherwise({
            templateUrl: 'partials/subscribe.html',
            controller: 'subscribeController',

        });
});

/* push notification functions*/
function onNotificationGCM(e) {
    switch (e.event) {
        case 'registered':
            if (e.regid.length > 0) {
                var gcmid = e.regid;
                var s = document.getElementById('gcmid');
                s.value = gcmid;
                //alert(gcmid);
            }
            break;

        case 'message':
            if (e.foreground) {
            }
            else {
            }

            break;
        case 'error':

            break;

        default:
            break;
    }
}

function tokenHandler(result) {
}

function successHandler(result) {

}

function errorHandler(error) {

}

function onDeviceReady() {
    if (localStorage.getItem('registerid') == 0 || localStorage.getItem("registerid") === null) {
        pushNotification = window.plugins.pushNotification;
        if (device.platform == 'android' || device.platform == 'Android') {
            pushNotification.register(successHandler, errorHandler, {
                "senderID": "938400517847",
                "ecb": "onNotificationGCM"
            });
        }
    }
    else {

    }
}

document.addEventListener('deviceready', onDeviceReady, true);
/* end of push notification functions*/


/* called on every route*/
vrajApp.run(function ($rootScope, $localStorage, $http, $location) {

    $rootScope.subscribefooter = false;
    /* set title for all pages */
    $rootScope.title = function (title) {
        document.getElementById('brand-title').innerHTML = title;
    };

    /* set header for all pages */
    $rootScope.header = function (common) {
        $rootScope.defaultheader = common;

    };

    /* set footer for all pages */
    $rootScope.footer = function (common) {
        $rootScope.innerfooter = common;

    };

    $rootScope.subfooter = function (flag) {
        $rootScope.subscribefooter = flag;

    };

    $rootScope.$storage = $localStorage.$default({
        subscribe: 0
    });

    /* check for internet connection*/
    $rootScope.checkConnection = function () {
        return true;
        /*if(navigator.network.connection.type == Connection.NONE)
        {
            navigator.notification.alert('Please check your Internet Connection', function(){}, "Warning", "");
            $location.path('/home');
        }
        else
        {

        }*/

    };
    /* end of connection check */

    /* open gallery */
    $rootScope.opengallery = function (type, projectid) {

        if (type == 'f') {
            var fpath = 'http://www.vrajinfra.com/floorplangallery.php?projectid=' + projectid;
        }
        else if (type == 's') {
            var fpath = 'http://www.vrajinfra.com/sitegallery.php?projectid=' + projectid;
        }
        else if (type == 'p') {
            var fpath = 'http://www.vrajinfra.com/projectgallery.php?projectid=' + projectid;
        }

        var ref = window.open(fpath, '_blank', 'location=no');


        ref.addEventListener('loadstart', function (event) {
                navigator.notification.activityStart("Loading", "Please wait for a while.");
            }
        );
        ref.addEventListener('loadstop', function (event) {
            navigator.notification.activityStop();

        });

    };
    /* end of open gallery*/

    $rootScope.backfun = function () {
        $location.path('/home');
    }
    $rootScope.subtitle = function (title) {
        document.getElementById('sub-title').innerHTML = title;
    };
});


/* serailiaze data for sending to server */
function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p,
                v = obj[p];
            str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
}

vrajApp.controller('subscribeController', function ($scope, $http, $rootScope, $location, $window, $route) {
    $rootScope.header(false);
    $rootScope.subfooter(true);

    if ($rootScope.$storage.subscribe == 1) {
        $location.path('/home');

    }


    $scope.formInfo = {};

    $scope.submitTheForm = function (item, event) {
        if (navigator.network.connection.type == Connection.NONE) {
            navigator.notification.alert('Please check your Internet Connection', function () {
            }, "Warning", "");
            $route.reload();
        }
        else {

        }

        var gcmregid = document.getElementById("gcmid").value;

        localStorage.setItem('registerid', gcmregid);

        $scope.formInfo['gcmid'] = gcmregid;

        var serdata = serialize($scope.formInfo);
        console.log(serdata);
        /* get gcm id and insert with form details*/

        $http({
            url: serviceurl + "subscription.php",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: serdata
        }).success(function (data, status, headers, config) {
            if (data.msg == 1) {
                // alert('Your subscription has been done successfully!');
                navigator.notification.alert('Your subscription has been done successfully...', function () {
                }, "Success", "");
            }
            else {
                // alert('You are already subscribed to this application!');
                navigator.notification.alert('You are already subscribed to this application...', function () {
                }, "Already Applied", "");
            }
            $rootScope.$storage.subscribe = 1;
            $location.path("/home");

        }).error(function (data, status, headers, config) {


        });
    }

});

vrajApp.controller('mainController', function ($scope, $http, $rootScope, $location, $window) {

    $scope.latest = null;
    $rootScope.title('VRAJ INFRA');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);
    $scope.imgarray = ["img/app-slider1.jpg", "img/app-slider2.jpg", "img/app-slider3.jpg"];
    $scope.myInterval = 3000;
    document.addEventListener("backbutton", onBackKeyDown, false);

});

vrajApp.controller('aboutController', function ($scope, $http, $window, $rootScope, $location) {
    $rootScope.checkConnection();
    $rootScope.title('About Us');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);
    $scope.about = "";
    $scope.loader = true;
    $scope.$watch('loader', function (newvalue, oldvalue, $scope) {
        $scope.loader = newvalue;
    });

    $http.get(serviceurl + 'about.php')
        .success(function (data) {
            $scope.about = data;
            $scope.items = [
                {
                    name: "item1",
                    desc: "About Company",
                    information: $scope.about.company
                },
                {
                    name: "item2",
                    desc: "Our Mission",
                    information: $scope.about.mission
                },
                {
                    name: "item3",
                    desc: "Our Vision",
                    information: $scope.about.vision
                },
                {
                    name: "item4",
                    desc: "Our Team",
                    information: $scope.about.team
                },
                {
                    name: "item5",
                    desc: "Our Quality",
                    information: $scope.about.quality
                }

            ];
            $scope.default = $scope.items[1];
            $scope.$parent.isopen = ($scope.$parent.default === $scope.item);
            $scope.loader = false;
            $scope.$watch('isopen', function (newvalue, oldvalue, $scope) {
                $scope.$parent.isopen = newvalue;
            });
        })
        .error(function (data, status, headers, config) {
        });
    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('success-storyController', function ($scope, $rootScope, $http, $window, $location) {
    $rootScope.checkConnection();
    $rootScope.title('Success Stories');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);
    $scope.storyList = null;
    $scope.loader = true;
    $scope.$watch('loader', function (newvalue, oldvalue, $scope) {
        $scope.loader = newvalue;
    });

    $http.get(serviceurl + 'success-story.php')
        .success(function (data) {
            $scope.storyList = data;
            $scope.loader = false;
        })
        .error(function (data, status, headers, config) {
        });

    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('newsController', function ($scope, $rootScope, $http, $window, $location) {
    $rootScope.checkConnection();

    $rootScope.title('Our Timeline');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);
    $scope.newsList = null;
    $scope.loader = true;

    $scope.$watch('loader', function (newvalue, oldvalue, $scope) {
    });

    $http.get(serviceurl + 'news.php')
        .success(function (data) {
            $scope.newsList = data;
            $scope.loader = false;
        })
        .error(function (data, status, headers, config) {
        });
    document.removeEventListener("backbutton", onBackKeyDown, false);
});
vrajApp.controller('allprojectController', function ($scope, $http, $window, $rootScope, $location) {
    $rootScope.checkConnection();
    $rootScope.title('All Projects');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);

    $scope.loader = true;
    $scope.$watch('loader', function (newvalue, oldvalue, $scope) {
        $scope.loader = newvalue;
    });

    $scope.imgappend = '';
    if (window.innerWidth <= 480) {
        $scope.imgappend = "_mobile.jpg";
    } else {
        $scope.imgappend = "";
    }

    $scope.projectList = null;
    $http.get(serviceurl + 'producttype.php')
        .success(function (data) {
            $scope.projectcities = data;
            $scope.selectedcityId = $scope.projectcities[0].typeid;

            $http.get(serviceurl + 'projectlist.php')
                .success(function (prodata) {
                    $scope.projectList = prodata;
                    $scope.loader = false;
                }).error(function (data, status, headers, config) {
            });


        }).error(function (data, status, headers, config) {
    });

    $scope.changetype = function (id) {
        $scope.selectedcityId = id;
    }

    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('projectController', function ($scope, $http, $window, $rootScope, $location, $routeParams) {
    $rootScope.checkConnection();
    $scope.projectList = null;
    $scope.projectcategories = null;
    $scope.projectId = $routeParams.projectId;
    $scope.projectType = null;
    $scope.category = null;
    $scope.selectedcategoryId = '';

    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);

    $scope.loader = true;
    $scope.$watch('loader', function (newvalue, oldvalue, $scope) {
        $scope.loader = newvalue;
    });

    $scope.imgappend = '';
    if (window.innerWidth <= 480) {
        $scope.imgappend = "_mobile.jpg";
    } else {
        $scope.imgappend = "";
    }

    var dataObject =
        {
            typeid: $scope.projectId
        };

    // serialize data before sending
    var serdata = serialize(dataObject);

    $http({
        url: serviceurl + "projectlist.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    }).success(function (data, status, headers, config) {
        $scope.projectlist = data;
        $rootScope.title(data[0].typeparent);

        $http({
            url: serviceurl + "producttype.php",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: serdata
        }).success(function (categorydata, status, headers, config) {
            $scope.projectcategories = categorydata;
            $scope.selectedcategoryId = $scope.projectcategories[0].typeid;
            $scope.category = $scope.projectcategories[0].typetitle;
            $scope.loader = false;

        }).error(function (data, status, headers, config) {
        });

    }).error(function (data, status, headers, config) {
    });

    $scope.changetype = function (id) {
        $scope.selectedcategoryId = id;
    }

    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('contactController', function ($scope, $http, $route, $window, $rootScope, $location) {
    $rootScope.title('Inquiry');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);
    $scope.loader = true;
    $rootScope.checkConnection();
    setTimeout(function () {
        $scope.$apply(function () {
            $scope.loader = false;
        });
    }, 2000);

    $scope.formInfo = {};

    $scope.submitTheForm = function (item, event) {

        var serdata = serialize($scope.formInfo);

        //serialize data before sending

        $http({
            url: serviceurl + "inquiry.php",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: serdata
        }).success(function (data, status, headers, config) {
            if (data.msg == '1') {
                navigator.notification.alert('Your inquiry has been send successfully...We will be in touch with you Soon...', function () {
                }, "Success", "");
            }
            else {

            }

            $route.reload();
        }).error(function (data, status, headers, config) {
            navigator.notification.alert('Submitting form failed!', function () {
            }, "Failure", "");
        });
    }

    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('reachusController', function ($scope, $rootScope, $location) {
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);

    $rootScope.title('Reach Us');
    $rootScope.checkConnection();

    $scope.items = [
        {
            name: "item1",
            desc: "Rajkot",
            content: " <b>Vraj Infrastructure Pvt. Ltd. </b></br>" +
            "Opp. Balaji Hall, <br> Nr. Jaysan Marble, <br> 150ft. Ring Road, Rajkot, <br> Gujarat, 360005 India.<br>" +

            "<i class='fa fa-phone'></i>&nbsp;+91 8100052000 </br>" +
            "<i class='fa fa-envelope'></i><a href='mailto:contact@vrajinfra.com' style='display:  initial;color: #444;'>contact@vrajinfra.com</a></br>"
            /*content: "Vallabh Vidyanagar, Street No.1,<br>Balaji Hall, 150ft. Ring Road,<br>Rajkot-360005,Gujarat,India.<br><i class='fa fa-phone'>&nbsp;&nbsp;</i>+91 0281 6196660<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@vrajinfra.com"*/

        }/*,
        {
            name: "item2",
            desc: "Jetpur",
            content: "127,Gokul Nagar, Opp Balkrishna Farm,<br>National Highway 8-B,Jetpur-360370,<br>Gujarat, India. <br><i class='fa fa-phone'>&nbsp;&nbsp;</i>+91 9925784000<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@vrajinfra.com"

        },
        {
            name: "item3",
            desc: "Junagadh",
            content: "Panch Hatadi Chowk,<br>Vallabhasherya Complex, Mangnath Road,<br>Junagadh-362001, Gujarat, India. <br><i class='fa fa-phone'>&nbsp;&nbsp;</i> +91 9925784000<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@vrajinfra.com"

        },
        {
            name: "item4",
            desc: "Dubai",
            content: "Gold Souk Central Market, <br>Block No.1.Shop No : 33 & 34, <br>P.O.Box : 21564, Sharjah-U.A.E.<br><i class='fa fa-phone'>&nbsp;&nbsp;</i> +971 6-5731961<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i> alabyadjewellers.com <br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@vrajinfra.com"

        },

        {
            name: "item5",
            desc: "London",
            content: "Dhaval Kishore Parmar,<br>20 The Avenue, Wembley,<br>Middx HA9 9QJ, London, UK. <br><i class='fa fa-phone'>&nbsp;&nbsp;</i> 075321 86622<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@vrajinfra.com"

        }*/,
        {
            name: "item7",
            desc: "Social Connects",
            content: "<a class='social' href='' onclick = openlinks('https://www.facebook.com/pages/Vraj-infrastructure-pvtltd/1516625235224524') ><i class='icon fa fa-facebook'></i>&nbsp;&nbsp;Like Us on Facebook</a><br><a class='social' href='' onclick = openlinks('https://www.youtube.com/channel/UCxHx86DBDEMzRpo29gT3nDw')><i class='fa fa-youtube'></i>&nbsp;&nbsp;Follow Us on Youtube</a><br><p class='rate'>Love Vraj Infra ?<br>Help Us to make even better by rating the app!<br>Click on the stars to rate!<br><a href='https://play.google.com/store/apps/details?id=com.easternts.vrajinfra' class='heart-red' ><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i><i class='fa fa-star'>&nbsp;&nbsp;</i></a></p><hr><p class='support'>&copy; " + new Date().getFullYear() + " Vraj Infrastructure Pvt Ltd. All Rights Reserved.<br><span  class='color-red' href=''>www.vrajinfra.com</span><br></strong><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@vrajinfra.com <br><br>App Support : Eastern Techno Solutions<br><i class='fa fa-envelope'>&nbsp;&nbsp;</i>contact@easternts.com</p>"
        }
    ];

    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('searchprojectsController', function ($scope, $http, $rootScope, $location) {
    $rootScope.checkConnection();
    $rootScope.title('Search Projects');
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);

    $scope.loader = true;

    $scope.selectedparentid = 0;
    $scope.searchdata = null;

    $http.get(serviceurl + 'searchdetails.php')
        .success(function (data) {
            $scope.searchdata = data;
            $scope.selectedparentid = $scope.searchdata.parenttypes[0].typeid;
            $scope.selectedsubid = $scope.searchdata.subtypes[0].typeid;
            $scope.loader = false;
        }).error(function (data, status, headers, config) {
    });

    $scope.changeparent = function (id) {
        $scope.selectedparentid = id;

        for (i = 0; i < $scope.searchdata.subtypes.length; i++) {
            console.log($scope.searchdata.subtypes[i]);
            if ($scope.searchdata.subtypes[i].parentid == $scope.selectedparentid) {
                $scope.selectedsubid = $scope.searchdata.subtypes[i].typeid;
                break;
            }
        }
    }

    $scope.changesub = function (id) {
        $scope.selectedsubid = id;
    }

    $scope.submitTheForm = function () {
        $location.path("search/" + $scope.selectedsubid);
    }

    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('searchController', function ($scope, $http, $routeParams, $rootScope, $location) {
    $rootScope.checkConnection();
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);
    $scope.loader = true;

    $scope.id = $routeParams.id;

    if (window.innerWidth <= 480) {
        $scope.imgappend = "_mobile.jpg";
    }
    else {
        $scope.imgappend = "";
    }

    var dataObject =
        {
            typeid: $scope.id
        };

    // serialize data before sending
    var serdata = serialize(dataObject);

    $http({
        url: serviceurl + "searchproject.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    }).success(function (data, status, headers, config) {
        $scope.projects = data.projects;
        $scope.subcategory = data.subtitle;
        $scope.parentcategory = data.parenttitle;
        $scope.loader = false;

        var title = $scope.parentcategory + " - " + $scope.subcategory;

        $rootScope.title(title);

    }).error(function (data, status, headers, config) {
    });

});


vrajApp.controller('projectDetailController', function ($scope, $http, $routeParams, $rootScope, $location) {
    $rootScope.checkConnection();
    $rootScope.header(true);
    $rootScope.footer(false);
    $rootScope.subfooter(false);
    $scope.loader = true;

    $rootScope.projectdetail = null;
    $scope.projectId = $routeParams.projectId;

    $rootScope.showoverview = true;
    $rootScope.showamenities = false;
    $rootScope.showspecification = false;
    $rootScope.showlocation = false;

    if (window.innerWidth <= 480) {
        $scope.imgappend = "_mobile.jpg";
    } else {
        $scope.imgappend = "";
    }

    var dataObject =
        {
            projectid: $scope.projectId
        };

    // serialize data before sending
    var serdata = serialize(dataObject);

    $http({
        url: serviceurl + "projectdetail.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    }).success(function (data, status, headers, config) {
        $rootScope.projectdetail = data;

        $rootScope.title($rootScope.projectdetail.title);
        $scope.loader = false;

    }).error(function (data, status, headers, config) {
    });

    $rootScope.downloadbrochure = function () {
        navigator.notification.confirm(
            'Do You want to download the brochure?',
            function (button) {
                if (button == 1) {
                    window.openFileNative.open($rootScope.projectdetail.brochure);
                }
            },
            'Brochure Download',
            'Yes,No'
        );
    }

    $rootScope.downloadlocation = function () {
        navigator.notification.confirm(
            'Do You want to download the location?',
            function (button) {
                if (button == 1) {
                    window.openFileNative.open($rootScope.projectdetail.location);
                }
            },
            'Location Download',
            'Yes,No'
        );
    }

    $rootScope.showcontent = function (id) {
        if (id == 1) {
            $rootScope.showoverview = true;
            $rootScope.showamenities = false;
            $rootScope.showspecification = false;
            $rootScope.showlocation = false;
        }
        else if (id == 2) {
            $rootScope.showoverview = false;
            $rootScope.showamenities = false;
            $rootScope.showspecification = true;
            $rootScope.showlocation = false;
        }
        else if (id == 3) {
            $rootScope.showoverview = false;
            $rootScope.showamenities = true;
            $rootScope.showspecification = false;
            $rootScope.showlocation = false;
        }
        else if (id == 4) {
            $rootScope.showoverview = false;
            $rootScope.showamenities = false;
            $rootScope.showspecification = false;
            $rootScope.showlocation = true;
        }
        else {
            $rootScope.showoverview = true;
            $rootScope.showamenities = false;
            $rootScope.showspecification = true;
        }
    };

    $rootScope.playvideo = function () {

        var videoid = $rootScope.projectdetail.videourl;
        YoutubeVideoPlayer.openVideo(videoid);
    }

    $rootScope.shareproject = function () {
        var overviewtxt = removeHTMLTags();
        var imgsrc = $rootScope.projectdetail.gallery;
        // alert(imgsrc + " " + $rootScope.projectdetail.title + " " + overviewtxt + " " + $rootScope.projectdetail.weburl);
        window.plugins.socialsharing.share("Ayushi", $rootScope.projectdetail.title, imgsrc, $rootScope.projectdetail.weburl);
    }


    document.removeEventListener("backbutton", onBackKeyDown, false);
});

vrajApp.controller('notificationController', function ($scope, $http, $window, $rootScope, $location) {
    $rootScope.header(true);
    $rootScope.footer(true);
    $rootScope.subfooter(false);

    $rootScope.title("Notifications");
    $scope.loader = true;

    setTimeout(function () {
        $rootScope.checkConnection();
        $scope.notificationList = null;
        var dataObject = {
            enabled: '1'
        };
        // serialize data before sending
        var serdata = serialize(dataObject);
        $http({
            url: serviceurl + "notification.php",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: serdata
        }).success(function (data, status, headers, config) {
            $scope.notificationList = data;
            $scope.loader = false;
        }).error(function (data, status, headers, config) {

        });

    }, 2000);


    document.removeEventListener("backbutton", onBackKeyDown, false);
});



